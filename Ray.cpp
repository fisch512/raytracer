#include "Ray.h"

Ray::Ray(double x, double y, double z, double u, double v, double w) {
    cs5721::Vector3D vector;
    vector.set(x, y, z);
    origin = vector;
    vector.set(u, v, w);
    direction = vector;
    direction.normalize();
}

Ray::Ray(cs5721::Vector3D o, cs5721::Vector3D d) {
    origin = o;
    direction = d;
    direction.normalize();
}

void Ray::setDirection(const cs5721::Vector3D d) {
    direction = d;
    direction.normalize();
}

void Ray::setOrigin(const cs5721::Vector3D o) {
    origin = o;
}

cs5721::Vector3D &Ray::getOrigin() {
    return origin;
}
cs5721::Vector3D &Ray::getDirection() {
    return direction;
}


