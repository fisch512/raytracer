/* 
 * File:   BlinnPhongShader.h
 * Author: matt
 *
 * Created on February 12, 2011, 4:47 PM
 */

#ifndef BLINNPHONGSHADER_H
#define	BLINNPHONGSHADER_H

#include "Shader.h"

class BlinnPhongShader : public Shader {
private:
    cs5721::Vector3D surfaceColor;
    cs5721::Vector3D specularColor;
    double phongExp, mirror;
public:
    BlinnPhongShader(double&, double&, double&, double&, double&, double&, double&, double&);
    cs5721::Vector3D calcColor(cs5721::Vector3D, cs5721::Vector3D,
            cs5721::Vector3D, cs5721::Vector3D);
    cs5721::Vector3D getSurfaceColor();
    double getMirrorCoef();
};

#endif	/* BLINNPHONGSHADER_H */

