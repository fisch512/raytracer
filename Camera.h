/* 
 * File:   Camera.h
 * Author: matt
 *
 * Created on February 3, 2011, 6:45 PM
 */

#ifndef CAMERA_H
#define	CAMERA_H

#include "Vector3D.h"
#include <vector>

class Camera {
private:
    cs5721::Vector3D position;
    cs5721::Vector3D viewDirection;
    double focalLength;
    double imagePlaneWidth;
    void calculateOrthonormalFrame();

public:

    Camera(double, double, double, double,
            double, double, double, double);

    cs5721::Vector3D getPosition();
    cs5721::Vector3D getViewDirection();
    double getFocalLength();
    double getImagePlaneWidth();
    std::vector<cs5721::Vector3D> orthonormalFrame;

};


#endif	/* CAMERA_H */

