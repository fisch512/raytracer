/* 
 * File:   LambertianShader.h
 * Author: matt
 *
 * Created on February 12, 2011, 4:46 PM
 */

#ifndef LAMBERTIANSHADER_H
#define	LAMBERTIANSHADER_H

#include "Shader.h"

class LambertianShader : public Shader {
private:
    cs5721::Vector3D surfaceColor;
public:
    LambertianShader(double, double, double);
    cs5721::Vector3D calcColor(cs5721::Vector3D, cs5721::Vector3D,
            cs5721::Vector3D, cs5721::Vector3D);
    cs5721::Vector3D getSurfaceColor();
    double getMirrorCoef();
};

#endif	/* LAMBERTIANSHADER_H */

