#include "Camera.h"

using namespace std;

Camera::Camera(double x, double y, double z, double u,
        double v, double w, double fl, double ipw) {
    position.set(x, y, z);
    viewDirection.set(u, v, w);
    focalLength = fl * 2; // fl * 2 for scene files
    imagePlaneWidth = ipw;
    calculateOrthonormalFrame();
}

cs5721::Vector3D Camera::getPosition() {
    return position;
}

cs5721::Vector3D Camera::getViewDirection() {
    return viewDirection;
}

double Camera::getFocalLength() {
    return focalLength;
}

double Camera::getImagePlaneWidth() {
    return imagePlaneWidth;
}

void Camera::calculateOrthonormalFrame() {
    cs5721::Vector3D U, V, W, B;

    W = viewDirection * -1.0;
    W.normalize();

    if(W[0] == 0.0 && W[1] == 1.0 && W[2] == 0.0) {
        cout << "TODO: view direction is strait down - FIX: Camera.calculateOrthonormal" << endl;
    } else {
        B.set(0.0, 1.0, 0.0);
    }

    U = B.cross(W);
    U.normalize();

    V = W.cross(U);
    V.normalize();

    orthonormalFrame.push_back(U);
    orthonormalFrame.push_back(V);
    orthonormalFrame.push_back(W);
}


