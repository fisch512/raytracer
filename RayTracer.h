/* 
 * File:   RayTracer.h
 * Author: matt
 *
 * Created on February 3, 2011, 6:13 PM
 */

#ifndef RAYTRACER_H
#define	RAYTRACER_H

#include <vector>
#include <string>
#include "Camera.h"
#include "Light.h"
#include "Surface.h"

class RayTracer {
private:
    std::string inputFileName;
    std::string outputFileName;
    double imageWidth;
    double imageHeight;
    std::vector<Camera> cameras;
    std::vector<Light> lights;
    std::vector<Surface*> surfaces;
    float *imageData;


    void readSceneFile();
    void processSphere(std::string);
    void processTriangle(std::string);
    void processCamera(std::string);
    void processLight(std::string);

public:
    RayTracer(int, char **);
    //void render();
    void start();

    struct scene {
        double imageHeight;
        double imageWidth;
        std::vector<Camera> cameras;
        std::vector<Light> lights;
        std::vector<Surface*> surfaces;
        float *imageData;
        double start;
        double finish;
        bool timer;
    };
};

static void *render(void *);
static cs5721::Vector3D rayTrace(Ray, int, RayTracer::scene*);
static bool inShadow(Ray, RayTracer::scene*);


#endif	/* RAYTRACER_H */

