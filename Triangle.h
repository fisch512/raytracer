/*
 * File:   Triangle.h
 * Author: matt
 *
 * Created on February 3, 2011, 7:45 PM
 */

#ifndef TRIANGLE_H
#define	TRIANGLE_H

#include "Surface.h"
#include "Shader.h"

class Triangle: public Surface {
private:
    cs5721::Vector3D vertex1;
    cs5721::Vector3D vertex2;
    cs5721::Vector3D vertex3;
    Shader* shader;

public:
    Triangle(double, double, double, double,
            double, double, double, double, double, Shader*);
    bool isHit(Ray, double, double, double &);
    cs5721::Vector3D getNormal(cs5721::Vector3D);
    Shader* getShader();
    cs5721::Vector3D getColor();
};


#endif	/* TRIANGLE_H */

