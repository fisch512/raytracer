/* 
 * File:   Ray.h
 * Author: matt
 *
 * Created on February 3, 2011, 8:33 PM
 */

#ifndef RAY_H
#define	RAY_H

#include "Vector3D.h"

class Ray {
private:
    cs5721::Vector3D origin;
    cs5721::Vector3D direction;
public:
    Ray(double, double, double, double, double, double);
    Ray(cs5721::Vector3D, cs5721::Vector3D);
    cs5721::Vector3D& getDirection();
    cs5721::Vector3D& getOrigin();
    void setDirection(const cs5721::Vector3D);
    void setOrigin(const cs5721::Vector3D);
};

#endif	/* RAY_H */

