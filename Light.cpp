#include "Light.h"

using namespace std;

Light::Light(double x, double y, double z,
        double r, double g, double b) {
    position.set(x, y, z);
    intensity.set(r, g, b);
}

cs5721::Vector3D Light::getPosition() {
    return position;
}

cs5721::Vector3D Light::getIntensity() {;
    return intensity;
}