#include "LambertianShader.h"
#include "Constants.h"
#include <algorithm>

using namespace std;

LambertianShader::LambertianShader(double r, double g, double b) {
    surfaceColor.set(r, g, b);
}

cs5721::Vector3D LambertianShader::calcColor(cs5721::Vector3D i,
        cs5721::Vector3D l, cs5721::Vector3D n, cs5721::Vector3D v) {
    cs5721::Vector3D color, intensity, light, normal, view;
    double red, green, blue;

    intensity = i;
    light = l;
    normal = n;
    view = v;


//    red = surfaceColor[0] * ambient;
//    green = surfaceColor[1] * ambient;
//    blue = surfaceColor[2] * ambient;


    red += surfaceColor[0] * intensity[0] * max(0.0, normal.dot(light));
    green += surfaceColor[1] * intensity[1] * max(0.0, normal.dot(light));
    blue += surfaceColor[2] * intensity[2] * max(0.0, normal.dot(light));

    color.set(red, green, blue);

    return color;

}

cs5721::Vector3D LambertianShader::getSurfaceColor() {
    return surfaceColor;
}

double LambertianShader::getMirrorCoef() {
    return 0.0;
}