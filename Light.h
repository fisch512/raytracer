/* 
 * File:   Light.h
 * Author: matt
 *
 * Created on February 3, 2011, 6:45 PM
 */

#ifndef LIGHT_H
#define	LIGHT_H

#include "Vector3D.h"


class Light {
private:
    cs5721::Vector3D position;
    cs5721::Vector3D intensity;
public:
    Light(double, double, double,
            double, double, double);
    cs5721::Vector3D getPosition();
    cs5721::Vector3D getIntensity();
};


#endif	/* LIGHT_H */

