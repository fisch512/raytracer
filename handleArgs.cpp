/*
 *  handleArgs.cpp
 *
 *  Created by Pete Willemsen on 10/6/09.
 *  Copyright 2009 Department of Computer Science, University of Minnesota-Duluth. All rights reserved.
 *
 * This file is part of CS5721 Computer Graphics library (cs5721Graphics).
 *
 * cs5721Graphics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cs5721Graphics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cs5721Graphics.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "handleArgs.h"

using namespace cs5721;

GraphicsArgs::GraphicsArgs()
  : verbose(false), width(100), height(100), inputFileName(""), outputFileName("")
{
}

void GraphicsArgs::process(int argc, char *argv[])
{
  ArgumentParsing argParser;

  argParser.reg("help", '?', no_argument);
  argParser.reg("verbose", 'v', no_argument);
  argParser.reg("inputfile", 'i', required_argument);
  argParser.reg("outputfile", 'o', required_argument);
  argParser.reg("width", 'w', required_argument);
  argParser.reg("height", 'h', required_argument);

  argParser.processCommandLineArgs(argc, argv);

  if (argParser.isSet("help"))
    {
      std::cout << "Usage: " << argv[0] << std::endl
		<< " [--verbose | -v]       --> turn on verbose output" << std::endl
		<< " [--width | -w]         --> width of image (default is 100)"  << std::endl
		<< " [--height | -h]        --> height of image (default is 100)"  << std::endl
		<< " [--inputfile | -i]     --> input file name to use" << std::endl
		<< " [--outputfile | -o]    --> output file name to use" << std::endl
		<< " [--help | -?]          --> this help/usage information"  << std::endl;
      exit(EXIT_SUCCESS);
    }

  if (argParser.isSet("verbose"))
    {
      verbose = true;
      std::cout << "Verbose Output: ON" << std::endl;
    }

  std::string argVal = "";

  if (argParser.isSet("width", argVal))
    {
      width = atoi(argVal.c_str());
      if (verbose) std::cout << "Setting width to " << width << std::endl;
    }

  if (argParser.isSet("height", argVal))
    {
      height = atoi(argVal.c_str());
      if (verbose) std::cout << "Setting height to " << height << std::endl;
    }
  
  if (argParser.isSet("inputfile", argVal))
    {
      inputFileName = argVal;
      if (verbose) std::cout << "Setting inputFileName to " << inputFileName << std::endl;
    }

  if (argParser.isSet("outputfile", argVal))
    {
      outputFileName = argVal;
      if (verbose) std::cout << "Setting outputFileName to " << outputFileName << std::endl;
    }
}

