/* 
 * File:   Sphere.h
 * Author: matt
 *
 * Created on February 3, 2011, 7:45 PM
 */

#ifndef SPHERE_H
#define	SPHERE_H

#include "Surface.h"
#include "Vector3D.h"
#include "Shader.h"

class Sphere: public Surface {
private:
    cs5721::Vector3D center;
    double radius;
    Shader *shader;
public:
    Sphere(double, double, double, double, Shader*);
    bool isHit(Ray, double, double, double &);
    cs5721::Vector3D getNormal(cs5721::Vector3D);
    Shader* getShader();
    cs5721::Vector3D getColor();
};


#endif	/* SPHERE_H */

