/* 
 * File:   Shader.h
 * Author: matt
 *
 * Created on February 12, 2011, 4:27 PM
 */

#ifndef SHADER_H
#define	SHADER_H

#include "Vector3D.h"

class Shader {
protected:
    

public:
    virtual cs5721::Vector3D calcColor(cs5721::Vector3D, cs5721::Vector3D,
            cs5721::Vector3D, cs5721::Vector3D) = 0;
    virtual cs5721::Vector3D getSurfaceColor() = 0;
    virtual double getMirrorCoef() = 0;
};

#endif	/* SHADER_H */

