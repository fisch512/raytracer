/*
 *  PNGImage.h
 *
 *  Created by Pete Willemsen on 10/6/09.
 *  Copyright 2009 Department of Computer Science, University of Minnesota-Duluth. All rights reserved.
 *
 * This file is part of CS5721 Computer Graphics library (cs5721Graphics).
 *
 * cs5721Graphics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cs5721Graphics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cs5721Graphics.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PNGIMAGE_H__
#define __PNGIMAGE_H__ 1

#include <cstdio>
#include <string>

#include <setjmp.h>     /* for jmpbuf declaration in writepng.h */
#include "writepng.h"

namespace cs5721
{
  class PNGImage
  {
  public:
    PNGImage() {};

    bool writeFileData(const std::string& filename, const int width, const int height, const float *data);	
    
  private:
    void cleanup(void);
    mainprog_info m_png_fileinfo;
  };
}

#endif // __PNGIMAGE_H__
