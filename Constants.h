/* 
 * File:   Constants.h
 * Author: matt
 *
 * Created on February 15, 2011, 6:03 PM
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#define ambient 0.2
#define eps 0.0001
#define infinity 9999999.0
#define lambertian "lambertian"
#define blinnphong "blinnphong"
#define viewNormals false
#define maxDepth 20
#define percentOutput 1

#endif	/* CONSTANTS_H */

