#include "BlinnPhongShader.h"
#include "Constants.h"
#include "LambertianShader.h"
#include <algorithm>

using namespace std;

BlinnPhongShader::BlinnPhongShader(double& dR, double& dG, double& dB,
        double& sR, double& sG, double& sB, double& pExp, double& m) {
    surfaceColor.set(dR, dG, dB);
    specularColor.set(sR, sG, sB);
    phongExp = pExp;
    mirror = m;
}

cs5721::Vector3D BlinnPhongShader::calcColor(cs5721::Vector3D i,
        cs5721::Vector3D l, cs5721::Vector3D n, cs5721::Vector3D v) {
    cs5721::Vector3D color, intensity, light, normal, view, half;
    double red = 0.0, green = 0.0, blue = 0.0, maxNormalLight, maxNormalHalf;

    intensity = i;
    light = l;
    normal = n;
    view = v;

    half = view + light;
    half.normalize();

    maxNormalLight = max(0.0, normal.dot(light));
    maxNormalHalf = max(0.0, normal.dot(half));

    //Ambient shading
//    red = surfaceColor[0] * ambient;
//    green = surfaceColor[1] * ambient;
//    blue = surfaceColor[2] * ambient;

    //Lambertian shading
    red += surfaceColor[0] * intensity[0] * maxNormalLight;
    green += surfaceColor[1] * intensity[1] * maxNormalLight;
    blue += surfaceColor[2] * intensity[2] * maxNormalLight;

    //BlinnPhong shading
    red += specularColor[0] * intensity[0] * pow(maxNormalHalf, phongExp);
    green += specularColor[1] * intensity[1] * pow(maxNormalHalf, phongExp);
    blue += specularColor[2] * intensity[2] * pow(maxNormalHalf, phongExp);

    color.set(red, green, blue);

    return color;
}

cs5721::Vector3D BlinnPhongShader::getSurfaceColor() {
    return surfaceColor;
}

double BlinnPhongShader::getMirrorCoef() {
    return mirror;
}