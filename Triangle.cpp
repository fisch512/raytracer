#include "Triangle.h"
#include "Constants.h"

Triangle::Triangle(double x1, double y1, double z1,
        double x2, double y2, double z2,
        double x3, double y3, double z3, Shader* s) {
    vertex1.set(x1, y1, z1);
    vertex2.set(x2, y2, z2);
    vertex3.set(x3, y3, z3);
    shader = s;
}

bool Triangle::isHit(Ray r, double tmin, double tmax, double &t) {
    cs5721::Vector3D rDirection = r.getDirection();
    cs5721::Vector3D rOrigin = r.getOrigin();
    double a, b, c, d, e, f, g, h, i, j, k, l;
    double eiMhf, gfMdi, dhMeg, akMjb, jcMal, blMkc;
    double T, Y, B, M;
    double v1X = vertex1[0];
    double v1Y = vertex1[1];
    double v1Z = vertex1[2];


    a = v1X - vertex2[0];
    b = v1Y - vertex2[1];
    c = v1Z - vertex2[2];

    d = v1X - vertex3[0];
    e = v1Y - vertex3[1];
    f = v1Z - vertex3[2];

    g = rDirection[0];
    h = rDirection[1];
    i = rDirection[2];

    j = v1X - rOrigin[0];
    k = v1Y - rOrigin[1];
    l = v1Z - rOrigin[2];

    eiMhf = e * i - h*f;
    gfMdi = g * f - d*i;
    dhMeg = d * h - e*g;
    akMjb = a * k - j*b;
    jcMal = j * c - a*l;
    blMkc = b * l - k*c;

    M = a * eiMhf + b * gfMdi + c * dhMeg;
    T = -((f * akMjb + e * jcMal + d * blMkc) / M);
    Y = (i * akMjb + h * jcMal + g * blMkc) / M;
    B = (j * eiMhf + k * gfMdi + l * dhMeg) / M;

    if (T < tmin || T > tmax) {
        return false;
    }

    Y = (i * akMjb + h * jcMal + g * blMkc) / M;
    if (Y < 0.0 || Y > 1.0) {
        return false;
    }

    B = (j * eiMhf + k * gfMdi + l * dhMeg) / M;
    if (B < 0.0 || B > (1.0 - Y)) {
        return false;
    }

    t = T;
    return true;
}

cs5721::Vector3D Triangle::getNormal(cs5721::Vector3D pointHit) {
    cs5721::Vector3D normal;
    normal = (vertex2 - vertex1).cross(vertex3 - vertex1);
    normal.normalize();
    return normal;
}

Shader* Triangle::getShader() {
    return shader;
}

cs5721::Vector3D Triangle::getColor() {
    return shader->getSurfaceColor();
}