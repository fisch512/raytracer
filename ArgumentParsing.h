/*
 *  ArgumentParsing.h
 *
 *  Created by Pete Willemsen on 10/6/09.
 *  Copyright 2009 Department of Computer Science, University of Minnesota-Duluth. All rights reserved.
 *
 * This file is part of CS5721 Computer Graphics library (cs5721Graphics).
 *
 * cs5721Graphics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cs5721Graphics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cs5721Graphics.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARGUMENT_PARSING_H__
#define __ARGUMENT_PARSING_H__ 1

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <vector>

#include <getopt.h>

namespace cs5721
{
  class ArgumentParsing
  {
  public:
    ArgumentParsing();
    ArgumentParsing(int argc, char *argv[]);
    ~ArgumentParsing();

    void reg(const std::string& argName, char shortArgName, int has_argument, bool required=false);
    int processCommandLineArgs(int argc, char *argv[]) { return process(argc, argv); }

    bool isSet(const std::string& argName);
    bool isSet(const std::string& argName, std::string &argValue);

  protected:
    int process(int argc, char *argv[]);
    int lookupIndex(char c);
	
  private:

    struct ModifiedOption 
    {
      option optParams;
      bool isSet;
      std::string optionalArgument;
    };
   
    std::vector<ModifiedOption> m_ArgVector;
  };
}

#endif // __ARGUMENT_PARSING_H__ 1
